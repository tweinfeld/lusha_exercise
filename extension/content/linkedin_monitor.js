const
    SECOND = 1000,
    URL_SAMPLE_INTERVAL = 2 * SECOND;

const
    port = chrome.runtime.connect(),
    $textFactory = (document)=> (selector)=> {
        const el = document.querySelector(selector);
        return el ? (el.textContent || "").trim() : null;
    },
    scrapers = [
        {
            "url_test": (url)=> /^https?:\/\/www\.linkedin\.com\/in\/[^/]+\/?$/.test(url),
            "scraper": ({ document })=> {
                const $text = $textFactory(document);
                return Kefir
                    .constant([
                        "h1.pv-top-card-section__name",
                        "div.pv-top-card-section__distance-badge",
                        "h3.pv-top-card-section__company",
                        "h3.pv-top-card-section__location",
                        "h2.pv-top-card-section__headline",
                        "h3.pv-top-card-section__connections"
                        ].reduce((ac, selector, index)=> {
                            const value = $text(selector);
                            return Object.assign(ac, value && { [["full_name", "degree", "company", "locality", "headline", "connections"][index]]: value });
                        }, {})
                    );
            }
        },
        {
            "url_test": ()=> true,
            "scraper": ()=> Kefir.never()
        }
    ],
    queryUrl = ()=> document.URL,
    urlProperty = Kefir
        .fromPoll(URL_SAMPLE_INTERVAL, queryUrl)
        .toProperty(queryUrl)
        .skipDuplicates();

const contactStream = urlProperty
    .flatMapLatest((url)=> scrapers.find(({ url_test })=> url_test(url))["scraper"](window));

Kefir
    .merge([
        urlProperty.map((url)=> ({ type: "url_change", value: url })),
        contactStream.map((contact)=> ({ type: "contact_scrape", value: contact }))
    ])
    .onValue(port.postMessage.bind(port));