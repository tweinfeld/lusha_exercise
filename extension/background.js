const
    contentScriptStream = streamFromEvents(chrome.runtime.onConnect)
        .filter((port)=> port.sender.tab)
        .flatMap((port)=> {
            const tabId = port.sender.tab.id;
            return streamFromEvents(port.onMessage)
                    .map((event)=> ({ event, tab: tabId }))
                    .takeUntilBy(streamFromEvents(port.onDisconnect).take(1))
                    .beforeEnd(()=> ({ event: { type: "tab_disconnect" }, tab: tabId }));
        });

const tabSearchProperty = contentScriptStream
    .filter(({ event: { type } })=> ["tab_disconnect", "url_change", "contact_scrape"].includes(type))
    .scan((searches, { event: { type, value }, tab })=> {
        return ["tab_disconnect", "url_change"].includes(type)
            ? searches.filter(({ tab: tabId })=> tabId !== tab)
            : searches.concat({ ...value, tab });
    }, []);

Kefir
    .combine([
        tabSearchProperty,
        streamFromEvents(chrome.runtime.onMessage)
    ], (searches)=> ({ type: "search", value: searches }))
    .onValue(chrome.runtime.sendMessage.bind(chrome.runtime, null));