const tabIdProperty = Kefir
    .fromCallback((cb)=> chrome.tabs.query({ active: true, currentWindow: true }, cb))
    .map(([{ id }])=> id);

chrome.runtime.sendMessage(null, { type: "query_search" });

Kefir
    .combine([
        tabIdProperty,
        streamFromEvents(chrome.runtime.onMessage).filter(({ type })=> type === "search").map(({ value })=> value),
    ])
    .onValue((function(){
        const
            [main, ul, li, div, span] = ["main", "ul", "li", "div", "span"].map((tagName)=> preact.h.bind(null, tagName)),
            [bodyEl, mainEl] = ["body", "body > main"].map(document.querySelector.bind(document)),
            contactTemplate = ({ full_name, headline })=> div({ className: "contact" }, [], ...[
                span({ className: "full_name" }, full_name),
                headline && span({ className: "headline" }, headline)
            ].filter(Boolean));

        return ([tabId, searches])=> {
            preact.render(
                main({}, [], ...[
                    searches.length ?
                        ul({}, _(searches)
                            .sortBy(({ tab, full_name })=> [tab === tabId ? 0 : 1, full_name])
                            .uniqBy('full_name')
                            .map(({ tab, ...contact }, index)=> li({ key: index, className: tabId === tab ? "current" : "" }, contactTemplate(contact)))
                            .value()
                        ) : div({ className: "nothing" }, 'Nothing here yet'),
                    (function(hitCount){ return hitCount && div({ className: "other_hits" }, `(${hitCount} on different tabs)`); })(_(searches).filter(({ tab })=> tab !== tabId).value().length)
                ].filter(Boolean)),
                bodyEl,
                mainEl
            );
        }
    })());